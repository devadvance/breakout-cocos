#ifndef __OPTIONS_SCENE_H__
#define __OPTIONS_SCENE_H__

#include "cocos2d.h"

using namespace cocos2d;

class Options : public cocos2d::CCLayer
{

private:
	float temp;

public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static CCScene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(Options);

    void soundOnButtonAction(CCObject* pSender);
    void soundOffButtonAction(CCObject* pSender);
    void menuButtonAction(CCObject* pSender);

};

#endif  __OPTIONS_SCENE_H__
