#include "MenuScene.h"
#include "HelloWorldScene.h"
#include "OptionsScene.h"
#include "SimpleAudioEngine.h"
#include "support/CCPointExtension.h"

using namespace cocos2d;
using namespace CocosDenshion;



CCScene* Menu::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    Menu *layer = Menu::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Menu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    /////////////////////////////

    CCSize winSize = CCDirector::sharedDirector()->getWinSize();





    // Create text label for title of game - "@stroids" - don't sue me Atari!
	CCLabelTTF *title = CCLabelTTF::labelWithString("Awesome Breakout Game", "Courier", 64.0);

	// Position title at center of screen
	title->setPosition(ccp(winSize.width / 2, winSize.height/4*3));

	// Add to layer
	this->addChild(title, 1);

	// Set the default CCMenuItemFont font
	CCMenuItemFont::setFontName("Courier");

	// Create "play," "scores," and "controls" buttons - when tapped, they call methods we define: playButtonAction and scoresButtonAction
	CCMenuItemFont *playButton = CCMenuItemFont::create("Play", this, menu_selector(Menu::playButtonAction));
	CCMenuItemFont *optionsButton = CCMenuItemFont::create("Options", this, menu_selector(Menu::optionsButtonAction));

	// Create menu that contains our buttons
	CCMenu *menu = CCMenu::menuWithItems(playButton, optionsButton, NULL);

	// Align buttons horizontally
	menu->alignItemsHorizontallyWithPadding(20);

	// Set position of menu to be below the title text
	menu->setPosition(ccp(winSize.width / 2, winSize.height / 2));

	// Add menu to layer
	this->addChild(menu, 2);

    return true;
}

void Menu::playButtonAction(CCObject* pSender)
{
    CCDirector::sharedDirector()->replaceScene(HelloWorld::scene());
}

void Menu::optionsButtonAction(CCObject* pSender)
{
    CCDirector::sharedDirector()->replaceScene(Options::scene());
}

