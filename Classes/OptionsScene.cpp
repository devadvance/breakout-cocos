#include "OptionsScene.h"
#include "MenuScene.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "support/CCPointExtension.h"

using namespace cocos2d;
using namespace CocosDenshion;



CCScene* Options::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // 'layer' is an autorelease object
    Options *layer = Options::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Options::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !CCLayer::init() )
    {
        return false;
    }

    /////////////////////////////

    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    // Create text label for title of game - "@stroids" - don't sue me Atari!
	CCLabelTTF *title = CCLabelTTF::labelWithString("Options", "Courier", 64.0);

	// Position title at center of screen
	title->setPosition(ccp(winSize.width / 2, winSize.height/4*3));

	// Add to layer
	this->addChild(title, 1);

	// Set the default CCMenuItemFont font
	CCMenuItemFont::setFontName("Courier");

	// Create "play," "scores," and "controls" buttons - when tapped, they call methods we define: playButtonAction and scoresButtonAction
	CCMenuItemFont *soundOnButton = CCMenuItemFont::create("Turn Sound On", this, menu_selector(Options::soundOnButtonAction));
	CCMenuItemFont *soundOffButton = CCMenuItemFont::create("Turn Sound Off", this, menu_selector(Options::soundOffButtonAction));
	CCMenuItemFont *menuButton = CCMenuItemFont::create("Return to Menu", this, menu_selector(Options::menuButtonAction));

	// Create menu that contains our buttons
	CCMenu *menu = CCMenu::menuWithItems(soundOnButton, soundOffButton, menuButton, NULL);
	CCLog("Here");
	// Align buttons horizontally
	menu->alignItemsHorizontallyWithPadding(20);
	CCLog("Here2");
	// Set position of menu to be below the title text
	menu->setPosition(ccp(winSize.width / 2, winSize.height / 2));

	// Add menu to layer
	this->addChild(menu, 2);
	CCLog("Here3");
    return true;
}

void Options::soundOnButtonAction(CCObject* pSender)
{
    CCUserDefault::sharedUserDefault()->setBoolForKey("soundOn", true);
}

void Options::soundOffButtonAction(CCObject* pSender)
{
	CCUserDefault::sharedUserDefault()->setBoolForKey("soundOn", false);
}

void Options::menuButtonAction(CCObject* pSender)
{
    CCDirector::sharedDirector()->replaceScene(Menu::scene());
}

