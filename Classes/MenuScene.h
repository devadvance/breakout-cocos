#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__

#include "cocos2d.h"

using namespace cocos2d;

class Menu : public cocos2d::CCLayer
{

private:
	float temp;

public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

    // there's no 'id' in cpp, so we recommand to return the exactly class pointer
    static CCScene* scene();

    // implement the "static node()" method manually
    CREATE_FUNC(Menu);

    void playButtonAction(CCObject* pSender);
    void optionsButtonAction(CCObject* pSender);
};

#endif  __MENU_SCENE_H__
